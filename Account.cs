﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinQPractice
{
    internal class Account
    {
        public double accountBalance;
        public List<Operations> operation;
        public Account()
        {
            this.accountBalance = 100+300+50-200-90;
            operation = new List<Operations>();
            operation.Add(new Operations(this.accountBalance, (this.accountBalance + 100),100,new DateTime(2022,01,01),OperationType.DEPOSIT));
            operation.Add(new Operations(this.accountBalance, (this.accountBalance + 300),300,new DateTime(2022,02,12),OperationType.DEPOSIT));
            operation.Add(new Operations(this.accountBalance, (this.accountBalance - 90),90,new DateTime(2022,03,23),OperationType.WITHDRAW));
            operation.Add(new Operations(this.accountBalance, (this.accountBalance + 50),50,new DateTime(2022,04,07),OperationType.DEPOSIT));
            operation.Add(new Operations(this.accountBalance, (this.accountBalance - 200),200,new DateTime(2022,05,27),OperationType.WITHDRAW));
        }


        public void Deposit(double amount)
        {
            operation.Add(new Operations(this.accountBalance, (this.accountBalance + amount),amount,default,OperationType.DEPOSIT));
            this.accountBalance += amount;
        }

        public void WithDraw(double amount)
        {
            if (amount > this.accountBalance)
                Console.WriteLine("insufficient funds");
            else
            {
                operation.Add(new Operations(this.accountBalance, (this.accountBalance - amount), amount, default, OperationType.WITHDRAW));
                this.accountBalance -= amount;
            }

        }

        public void ShowBalance()
        {
            Console.WriteLine($"You have: {this.accountBalance}.");
        }

        public void ShowOperations(double amount,OperationType operationType)
        {
            operation.Where(o => o.operationType == operationType && o.amount>=amount)
                     .OrderByDescending(o => o.dateTime).ToList().ForEach(o=>Console.WriteLine($"{o.operationType.ToString()}------>{o.amount}"));
        }
    }
}
