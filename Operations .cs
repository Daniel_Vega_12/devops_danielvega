﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinQPractice
{
    internal class Operations
    {
        public double accountBalanceInitial;
        public double accountBalanceFinal;
        public double amount;
        public DateTime dateTime;
        public OperationType operationType;
        public Operations(double accountBalanceInitial, double accountBalanceFinal, double amount, DateTime dateTime,OperationType operation)
        {
            this.accountBalanceInitial = accountBalanceInitial;
            this.accountBalanceFinal=accountBalanceFinal; 
            this.amount = amount;
            this.dateTime = dateTime == default ? DateTime.Now : dateTime;
            this.operationType = operation;
        }

    }
}
