﻿using LinQPractice;
// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");
Account account = new Account();
string op;
do
{
    Console.WriteLine("What do you want to do?");
    Console.WriteLine("1. Deposit");
    Console.WriteLine("2. WithDraw");
    Console.WriteLine("3. ShowBalance");
    Console.WriteLine("4. ShowOperations");
    Console.WriteLine("Q. EXIT");
    op=Console.ReadLine();
    double amount;
    if (op == "Q" || op == "q") break;
    switch(op)
    {
        case "1":
            Console.WriteLine("How many do you want to deposit");
            amount = double.Parse(Console.ReadLine());
            account.Deposit(amount);
            break;
        case "2":
            Console.WriteLine("How many do you want to withdraw");
            amount = double.Parse(Console.ReadLine());
            account.WithDraw(amount);
            break;
        case "3":
            account.ShowBalance();
            break;
        case "4":
            Console.WriteLine("Amount filter:");
            amount = double.Parse(Console.ReadLine());
            Console.WriteLine("What kind of Operation do you want to see?");
            Console.WriteLine($"1. {OperationType.DEPOSIT}");
            Console.WriteLine($"2. {OperationType.WITHDRAW}");
            string aux = Console.ReadLine();
            account.ShowOperations(amount, aux == "1" ?OperationType.DEPOSIT:OperationType.WITHDRAW);
            break;
        default:
            Console.WriteLine("Select other option");
            break;
    }
}while (true);


